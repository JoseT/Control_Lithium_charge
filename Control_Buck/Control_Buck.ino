
// #include <LiquidCrystal.h>
// LiquidCrystal lcd (30,31,32,33,34,35);
//Lazo anidado
volatile float KP_i = 0.025, KI_i = 12; 
volatile float KP_v = 1, KI_v = 40; 

volatile float Ts=1e-3;
volatile float E_v, uPI_v, Int_v, Ref_v = 4.2, Int_v_previo = 0;
volatile float E_i, uPI_i, Int_i, Ref_i = 0.8, Int_i_previo = 0;

// Parámetros del sistema antiwindup
volatile float dutyMax = 0.5;   // Valor máximo para el duty cycle
volatile float dutyMin = 0.1;   // Valor mínimo para el duty cycle
volatile float I_max = 1;         // Valor máximo para anti windup de corriente
volatile float I_min = 0.070;      // Valor mínimo para anti windup de corriente

// Definición de pines y variables globales
const int inputVoltagePin = A3;
const int inputCurrentPin = A0;
const int outputVoltagePin = A2;
const int outputCurrentPin = A1;
const int dutyPin = 2;

// Variables eléctricas y de control
volatile float inputVoltage = 0.0;
volatile float inputCurrent = 0.0;
volatile float outputVoltage = 0.0;
volatile float outputCurrent = 0.0;
volatile unsigned int DUTY;
volatile unsigned int aux;

// Parámetros de medición (calibración)
const float inputVoltageGain = 8.28;    // Ganancia para la tensión de entrada
const float inputCurrentGain = 0.3994;    // Ganancia para la corriente de entrada
const float outputVoltageGain = 1.768;   // Ganancia para la tensión de salida
const float outputCurrentGain = 1;       //Ganancia para la corriente de salida
const float inputCurrentOffset = 0.09835;    // Offset para la corriente de entrada
const float outputCurrentOffset = 0.280; //Offset para la corriente de salida
const float dutyGain = 1.0870;
const float dutyOffset = 0.1848;

// Variables de medición de tiempo (ms)
unsigned long ms=0;
unsigned long lastMeasurementTime = 0;

int MODO = 1; //1 es corriente cte. 2 es tension cte.

void configurar_PWM();
void configurar_timer2();
void PI_i_solo();
void PI_v_solo();
void PI_anidado();
void medicion_variables();

volatile float maxx(volatile float x, volatile float y){if(x > y){return x;}else{return y;}}
volatile float minn(volatile float x, volatile float y){if(x < y){return x;}else{return y;}}

void setup(){
  // Serial.begin(115200);
  // lcd.begin(16,2);
  // lcd.print("hola");
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT); 
  pinMode(13,OUTPUT);

  analogReference(INTERNAL2V56);

  configurar_PWM();
  
  DUTY = (int)205 * (dutyGain * 0.25 - dutyOffset); 
  // DUTY = (int)205 * 0.25;  
  OCR3B = DUTY;
  // digitalWrite(13,HIGH);
  // delay(5000);
  // digitalWrite(13,LOW);
  // delay(5000);  
  configurar_timer2();
}

void loop(){
  // if (ms == 10000)
  // {
  //   ms = 0;
  // }
}

/* En caso de prueba a ciclo fijo */
/*
ISR(TIMER2_COMPA_vect){

  // ms +=1;                           // Incremento variable de milisegundos transcurridos
  // digitalWrite(13,!digitalRead(13));
}
*/

/* Ejecución cada 1 ms del lazo de control */

ISR(TIMER2_COMPA_vect){
  ms +=1;                           // Incremento variable de milisegundos transcurridos
  medicion_variables(); //con la medicion se determina en que modo debe operar

  if (MODO==1) //esta en modo de corriente constante
  {
    //Lazo de corriente
    PI_i_solo(); 
    DUTY = (int)205 * (dutyGain * uPI_i - dutyOffset);
    // lcd.setCursor(0,1);
    // lcd.print(uPI_i);
  }
  else if (MODO==2) //esta en modo de tension constante
  {
    //Lazo anidado
    PI_anidado(); 
    digitalWrite(13,HIGH); 
    DUTY = (int)205 * (dutyGain * uPI_i - dutyOffset);
  }

  // //Lazo de tension 
  // medicion_variables();
  // PI_v_solo();  
  //DUTY = (int)205 * (dutyGain * uPI_v - dutyOffset);

  OCR3B = DUTY;
}


void PI_i_solo(void){

  E_i = Ref_i - outputCurrent;
  Int_i = (KI_i*E_i*Ts) + Int_i_previo;
  //Anti-windup
  Int_i = maxx(minn(Int_i,dutyMax), dutyMin);
  uPI_i = KP_i * E_i + Int_i;

  //Limitacion de salida
  uPI_i = maxx(minn(uPI_i,dutyMax), dutyMin);

  //Actualizacion de variables
  Int_i_previo = Int_i;
 
}


void PI_v_solo(void){

  E_v = Ref_v - outputVoltage;
  Int_v = (KI_v*E_v*Ts) + Int_v_previo;
  //Anti-windup
  Int_v = maxx(minn(Int_v,dutyMax), dutyMin);
  uPI_v = KP_v*E_v + Int_v;

  //Limitacion de salida
  uPI_v = maxx(minn(uPI_v,dutyMax), dutyMin);

  //Actualizacion de variables
  Int_v_previo = Int_v;
 
}

void PI_anidado(void){
  //Lazo de tension
  E_v = Ref_v - outputVoltage;
  Int_v = (KI_v * E_v * Ts) + Int_v_previo;
  //Anti-windup
  Int_v = maxx(minn(Int_v,I_max), I_min);
  uPI_v = KP_v * E_v + Int_v;

  //Limitacion de salida
  uPI_v = maxx(minn(uPI_v,I_max), I_min);

  //Actualizacion de variables
  Int_v_previo = Int_v;
  //--------------------------------
  //Lazo de corriente
  E_i = uPI_v - outputCurrent;
  Int_i = (KI_i * E_i * Ts) + Int_i_previo;
  Int_i = maxx(minn(Int_i,dutyMax), dutyMin);
  uPI_i = KP_i * E_i + Int_i;

  //Limitacion de salida
  uPI_i=maxx(minn(uPI_i,dutyMax), dutyMin);

  //Actualizacion de variables
  Int_i_previo = Int_i;
  //--------------------------------
}

/********************************/
/*** Medición y procesamiento ***/
/********************************/

void medicion_variables(void){

  // La medición  la variable demora aproximadamente 100us;
  int outputCurrentRaw = analogRead(outputCurrentPin);
  
  // Conversión de las lecturas a valores de corriente de salida
  outputCurrent = outputCurrentRaw * (2.56 / 1023.0);

  // Aplicación de la ganancia a la variable medida
  outputCurrent *= outputCurrentGain;
  outputCurrent -= outputCurrentOffset;

  // if ((outputCurrent<0.7) && (outputCurrent>0.3))
  // {
  //     digitalWrite(13,HIGH);
  // }
  // lcd.setCursor(0,0);
  // lcd.print(outputCurrent);

  int inputCurrentRaw = analogRead(inputCurrentPin);
  inputCurrent = inputCurrentRaw * (2.56 / 1023.0);
  inputCurrent *= inputCurrentGain;
  inputCurrent -= inputCurrentOffset;

  int outputVoltageRaw = analogRead(outputVoltagePin);
  outputVoltage = outputVoltageRaw * (2.56 / 1023.0);
  outputVoltage *= outputVoltageGain;

  if (outputVoltage>4.2)
  {
      digitalWrite(13,HIGH);
  }

  int inputVoltageRaw = analogRead(inputVoltagePin);
  inputVoltage = inputVoltageRaw * (2.56 / 1023.0);
  inputVoltage *= inputVoltageGain;

  //Configuracion de MODO segun valor de tension medida, implementacion de histeresis
  if (outputVoltage <= 4.0)
  {
    MODO = 1;
  }
  if (outputVoltage >= 4.2)
  {
    MODO = 2;
  }
  
}

/**********************************/
/*** Funciones de configuración ***/
/**********************************/

/* Configuración del generador de PWM a 39kHz */
void configurar_PWM(void) {
  pinMode(dutyPin, OUTPUT);             // Configuración del dutyPin como salida
  cli();                                // Deshabilitar interrupciones globales
  TCCR3A = (1 << COM3B1) | (1 << WGM30); // Configurar el registro TCCR3A en modo Phase Correct PWM Mode con salida en el pin 2
  TCCR3B = (1 << WGM33) | (1 << CS30);  // Configurar el Timer3 en modo Phase Correct PWM Mode y prescaler de 1
  OCR3A = 205;                          
  OCR3B = DUTY;                         // Establecer el valor de comparación OCR3B
  sei();                                // Habilitar interrupciones globales
}

void configurar_timer2(void){
  // cli();
  // Configura el Timer 2
  // Modo CTC (Clear Timer on Compare Match)
  TCCR2A = 0; // Configura el Timer 2 en modo normal (sin PWM)
  TCCR2B = (0 << CS22) | (1 << CS21) | (1 << CS20); // Configura el prescaler en 64

  // Configura el registro de comparación para interrupciones cada 1 ms
  OCR2A = 249; // (16MHz / 64) * 0.001s - 1

  // Habilita la interrupción de comparación A para Timer 2
  TIMSK2 |= (1 << OCIE2A);                              // Habilitar interrupciones globales
  // sei();
}




