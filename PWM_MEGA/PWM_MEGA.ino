const int dutyPin = 2; // Puedes cambiar esto a otro pin si lo deseas
volatile float DUTY;

void setup() {
  configurar_PWM();
  DUTY = (int)200 * 0.5;  // Ajustar el valor del ciclo de trabajo para la nueva frecuencia
  OCR3B = DUTY;
  delay(3000);
}

void loop() {
  // Tu código para el bucle principal, si es necesario
}

void configurar_PWM(void) {
  pinMode(dutyPin, OUTPUT);             // Configuración del dutyPin como salida
  cli();                                // Deshabilitar interrupciones globales
  TCCR3A = (1 << COM3B1) | (1 << WGM30); // Configurar el registro TCCR3A en modo Phase Correct PWM Mode con salida en el pin 2
  TCCR3B = (1 << WGM33) | (1 << CS30);  // Configurar el Timer3 en modo Phase Correct PWM Mode y prescaler de 1
  OCR3A = 200;                          // Reducir el valor de comparación OCR3A a la mitad para aumentar la frecuencia
  OCR3B = DUTY;                         // Establecer el valor de comparación OCR3B
  sei();                                // Habilitar interrupciones globales
}
