
%% Dise;o del convertidor
clc
clear all
format long
% 5v<Vs<20v
% 3v<Vo<4.2v
% 0.15<D<0.84

% Cond de L para mantener MCC
% L>(Vo*T*(1-D))/(Io*2)

% peor condicion
Vo = 4.2;
Vs = 20;
Io = 0.03*2200e-3; %0.03C
f = 80e6/2^11
T = 1/f; % entre 1k y 10k dependiendo de los bits de presicion
n = 90/100;%rendimiento
D = (Vo/Vs)/n;
L = (Vo*T*(1-D))/(Io*2)
% L = (R*T*(1-D))/(2)
% L = 1.36e-3;   %Bobina puesta en el convertidor

% D = 0.15; % peor condicion
C = ((1-D)*T^2)/(0.01*8*L)
% C = 2.2e-3;

% R=8/(0.8*2.2)
R=0.7;
L=1.36e-3;
C=47e-6;
Cbat=5;

% Modelo matematico de la planta

s = tf('s')
Ts = 1/83333 %entre 611 y 83333
Voee = 3.7;
Vs = 12;
Dee = Voee/Vs;
ILee = 600e-3;
R = Voee / ILee;
Wc = 2*pi*1.5e3;
Kp_I = 0.025;
Ki_I = 12;

% Gs = Vs * ((s*C)+(1/R))/((s^2*L*C)+(s*L/R)+1)
% Gz = c2d(Gs,Ts,'tustin')

Vref =4.2;
Iref=0.8;

%% Calculos para la construccion de L
L=130e-6;
I_DC = 0.8*2200e-3;
E = 0.5*L*I_DC^2;

% Nucleo E30
mu0 = 4*pi*10e-8;Ae = 60e-6; le=67e-3; mu=1600*mu0; AL=1800;
B = 0.2;

%Nucleo ---
% mu0 = 4*pi*10e-7;Ae = 60e-6; le=67e-3; mu=1600*mu0; AL=1800;
% B = 0.2;

% Cálculo del entrehierro
l0 = E*2*mu0/(B^2*Ae)

% Cálculo del número de vueltas
N=sqrt(L*l0/(Ae*mu0))


% N=sqrt(1.6e-3/(160e-6/(17^2)))
L= (160e-6 / (17^2)) * 63^2


%% Dise;o del PI de corriente

Gi = ((Voee/Dee)*(s*C+(1/R)))/(L*C*s^2+s*(L/R)+1)
Gc = Kp_I+(Ki_I/s)
Fc = Wc/(Wc+s);

pidTuner(Gi,Gc)

%% Calculos segun Texas
clear all
clc

Vout = 4.2;
Vin_max = 5;
Vin = 5;
Io_max = 0.8 * 2200e-3;
Io_min = 0.03 * 2200e-3;
fs = 80e6/2^11;
n = 0.9;
MaxD = Vout /(Vin_max * n);
% Delta_iL = (Vin_max - Vout) * MaxD / (fs * L)
Delta_iL = 0.2 * Io_max;
L = Vout * (Vin - Vout) / (Delta_iL * fs * Vin);
C = Delta_iL / (8 * fs * 0.1 * Vout)



%% Verificacion 
clear all
clc
Vcc_max = 20;
Res = Vcc_max / 2^11;
D = 4.2 / Vcc_max
Vcc_max * D * (1 - Res)

%% Interpolacion con polyfit
clear all
clc
set(0, 'DefaultAxesTickLabelInterpreter', 'latex');
set(0, 'DefaultTextInterpreter', 'latex');
set(0, 'DefaultLegendInterpreter', 'latex');
Ts = 1 / 40e3;
T_on_V_in = 2e-6:2e-6:16e-6;
% D_in = T_on_V_in / Ts;
T_on_V_diodo = [4.086e-6 6.5e-6 8.8e-6 10.96e-6 13.20e-6 15.55e-6 17.34e-6 19.11e-6];
% D_real = T_on_V_diodo / Ts;

D_in = [0.25 0.5];
D_real = [0.4 0.63];

ganancia = D_in / D_real
% plot(T_on_V_in,T_on_V_diodo)
% grid on
% xlabel('$T_{on}(V_{in})$')
% ylabel('$T_{on}(V_{diodo})$')
% figure()
% plot(D_in,D_real)
% grid on
% xlabel('$D_{in}$')
% ylabel('$D_{real}$')

%interpolando con polyfit
close all
clc
y = D_in;
x = D_real;
y1 = polyfit(x,y,1); %interpolo el D_in para saber que valor poner a la 
%entrada ante cierto valor de D_real que quiero
x = 0:0.1:1;
y = polyval(y1,x);
plot(x,y,'-k','LineWidth', 1.5) %ploteo al reves
grid on
hold on
plot(D_real,D_in, '.r', 'MarkerSize',20)
hold off
xlabel('$D_{real}$')
ylabel('$D_{in}$')
xlim([0 1])
ylim([0 1])
legend('Ajuste lineal','Mediciones')






