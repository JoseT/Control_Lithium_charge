import matplotlib.pyplot as plt
import numpy as np
f = open('prueba_carga_completa.log', 'r')
Io = []
Vo = []
while(True):
    Io.append(f.read(4))
    f.read(1)
    Vo.append(f.read(4))
    end = f.read(1)
    if(end != '\n'):
        break
Io.pop(-1)
Vo.pop(-1)

Io = [float(x) for x in Io]
Vo = [float(x) for x in Vo]
t_s = np.arange(0,len(Io),1)

# Io and Vo separate plots

plt.rcParams.update({'font.size': 22})
plt.figure(figsize=(20,20))
plt.title('Corriente de salida')
plt.xlabel('Tiempo [s]')
plt.ylabel('Io [A]')
plt.plot(t_s,Io)
plt.xlim([0,t_s[-1]])
plt.ylim([0,1])
plt.grid(True)
plt.show()

plt.figure(figsize=(20,20))
plt.plot(t_s,Vo)
plt.title('Tension de salida')
plt.xlabel('Tiempo [s]')
plt.ylabel('Vo [V]')
plt.xlim([0,t_s[-1]])
plt.ylim([3.5,4.5])
plt.grid(True)
plt.show()


# # Io and Vo on same figure
# plt.rcParams.update({'font.size': 22})
# fig, ax1 = plt.subplots()
# color = 'tab:red'
# ax1.set_xlabel('Tiempo [s]')
# ax1.set_ylabel('Vo [V]', color=color)
# ax1.plot(t_s, Vo, color=color)
# ax1.set_ylim([3.5,4.5])
# ax1.set_xlim([0,t_s[-1]])
# ax1.tick_params(axis='y', labelcolor=color)
# plt.grid(True)

# ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

# color = 'tab:blue'
# ax2.set_ylabel('Io [A]', color=color)  # we already handled the x-label with ax1
# ax2.plot(t_s, Io, color=color)
# ax2.tick_params(axis='y', labelcolor=color)

# fig.tight_layout()  # otherwise the right y-label is slightly clipped
# plt.show()